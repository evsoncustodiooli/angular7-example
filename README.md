# angular7-example

Versions:
Node:..........8.11.4
NPM:...........5.6.0
Angular/CLI:...7.3.9

## How To Replicate

Angular7 Exemplo - Gerado através do @angular/cli (ng)

1. Baixe e instale a versão do Node 8.11.4 ([https://nodejs.org/download/release/v8.11.4/](https://nodejs.org/download/release/v8.11.4)) para o seu sistema operacional, essa versão possui o NPM 5.6.0
2. Instale o Angular CLI globalmente via NPM: `npm install -g @angular/cli@7.3.9`
3. Gere o projeto base utilizando o `ng new angular7-example` aceitando a primeira pergunta (y) e selecionando SCSS

## Angular7Example

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
